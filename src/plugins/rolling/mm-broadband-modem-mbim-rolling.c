/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details:
 *
 * Copyright (C) 2024 Rolling Wireless S.a.r.l.
 */

#include <config.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#include "ModemManager.h"
#include "mm-log-object.h"
#include "mm-iface-modem.h"
#include "mm-iface-modem-3gpp.h"
#include "mm-broadband-modem-mbim-rolling.h"
#include "mm-shared-rolling.h"

static void iface_modem_init          (MMIfaceModem     *iface);
static void iface_modem_3gpp_init     (MMIfaceModem3gpp     *iface);
static void shared_rolling_init       (MMSharedRolling      *iface);
static void iface_modem_firmware_init (MMIfaceModemFirmware *iface);

static MMIfaceModem3gpp *iface_modem_3gpp_parent;

G_DEFINE_TYPE_EXTENDED (MMBroadbandModemMbimRolling, mm_broadband_modem_mbim_rolling, MM_TYPE_BROADBAND_MODEM_MBIM, 0,
                        G_IMPLEMENT_INTERFACE (MM_TYPE_IFACE_MODEM, iface_modem_init)
                        G_IMPLEMENT_INTERFACE (MM_TYPE_IFACE_MODEM_3GPP, iface_modem_3gpp_init)
                        G_IMPLEMENT_INTERFACE (MM_TYPE_SHARED_ROLLING,  shared_rolling_init)
                        G_IMPLEMENT_INTERFACE (MM_TYPE_IFACE_MODEM_FIRMWARE, iface_modem_firmware_init))

/******************************************************************************/

MMBroadbandModemMbimRolling *
mm_broadband_modem_mbim_rolling_new (const gchar  *device,
                                     const gchar  *physdev,
                                     const gchar **drivers,
                                     const gchar  *plugin,
                                     guint16       vendor_id,
                                     guint16       product_id)
{
    return g_object_new (MM_TYPE_BROADBAND_MODEM_MBIM_ROLLING,
                         MM_BASE_MODEM_DEVICE,     device,
                         MM_BASE_MODEM_PHYSDEV,    physdev,
                         MM_BASE_MODEM_DRIVERS,    drivers,
                         MM_BASE_MODEM_PLUGIN,     plugin,
                         MM_BASE_MODEM_VENDOR_ID,  vendor_id,
                         MM_BASE_MODEM_PRODUCT_ID, product_id,
                         /* MBIM bearer supports NET only */
                         MM_BASE_MODEM_DATA_NET_SUPPORTED, TRUE,
                         MM_BASE_MODEM_DATA_TTY_SUPPORTED, FALSE,
                         MM_IFACE_MODEM_SIM_HOT_SWAP_SUPPORTED, TRUE,
                         MM_IFACE_MODEM_PERIODIC_SIGNAL_CHECK_DISABLED, TRUE,
                         NULL);
}

static void
mm_broadband_modem_mbim_rolling_init (MMBroadbandModemMbimRolling *self)
{
}

static void
iface_modem_init (MMIfaceModem *iface)
{
    iface->reset        = mm_shared_rolling_modem_reset;
    iface->reset_finish = mm_shared_rolling_modem_reset_finish;
}

static void
iface_modem_3gpp_init (MMIfaceModem3gpp *iface)
{
    iface_modem_3gpp_parent = g_type_interface_peek_parent (iface);

    iface->set_initial_eps_bearer_settings        = mm_shared_rolling_set_initial_eps_bearer_settings;
    iface->set_initial_eps_bearer_settings_finish = mm_shared_rolling_set_initial_eps_bearer_settings_finish;
}

static void
iface_modem_firmware_init (MMIfaceModemFirmware *iface)
{
    iface->load_update_settings = mm_shared_rolling_firmware_load_update_settings;
    iface->load_update_settings_finish = mm_shared_rolling_firmware_load_update_settings_finish;
}

static MMBroadbandModemClass *
peek_parent_broadband_modem_class (MMSharedRolling *self)
{
    return MM_BROADBAND_MODEM_CLASS (mm_broadband_modem_mbim_rolling_parent_class);
}

static MMIfaceModem3gpp *
peek_parent_3gpp_interface (MMSharedRolling *self)
{
    return iface_modem_3gpp_parent;
}

static void
shared_rolling_init (MMSharedRolling *iface)
{
    iface->peek_parent_broadband_modem_class = peek_parent_broadband_modem_class;
    iface->peek_parent_3gpp_interface = peek_parent_3gpp_interface;
}

static void
mm_broadband_modem_mbim_rolling_class_init (MMBroadbandModemMbimRollingClass *klass)
{
    MMBroadbandModemClass *broadband_modem_class = MM_BROADBAND_MODEM_CLASS (klass);

    broadband_modem_class->setup_ports = mm_shared_rolling_setup_ports;
}
