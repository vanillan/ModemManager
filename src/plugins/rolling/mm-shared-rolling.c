/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details:
 *
 * Copyright (C) 2024 Rolling Wireless S.a.r.l.
 */

#include <config.h>
#include <arpa/inet.h>

#include <glib-object.h>
#include <gio/gio.h>

#define _LIBMM_INSIDE_MM
#include <libmm-glib.h>

#include "mm-log-object.h"
#include "mm-broadband-modem.h"
#include "mm-broadband-modem-mbim.h"
#include "mm-iface-modem.h"
#include "mm-iface-modem-3gpp.h"
#include "mm-shared-rolling.h"
#include "mm-base-modem-at.h"

/*****************************************************************************/
/* Private data context */

#define PRIVATE_TAG "shared-rolling-private-tag"
static GQuark private_quark;

typedef struct {
    /* Broadband modem class support */
    MMBroadbandModemClass *broadband_modem_class_parent;
    /* 3GPP interface support */
    MMIfaceModem3gpp *iface_modem_3gpp_parent;
    /* URCs to ignore */
    GRegex *sim_ready_regex;
} Private;

static void
private_free (Private *priv)
{
    g_regex_unref (priv->sim_ready_regex);
    g_slice_free (Private, priv);
}

static Private *
get_private (MMSharedRolling *self)
{
    Private *priv;

    if (G_UNLIKELY (!private_quark))
        private_quark = g_quark_from_static_string (PRIVATE_TAG);

    priv = g_object_get_qdata (G_OBJECT (self), private_quark);
    if (!priv) {
        priv = g_slice_new0 (Private);

        priv->sim_ready_regex = g_regex_new ("\\r\\n\\+SIM READY\\r\\n",
                                             G_REGEX_RAW | G_REGEX_OPTIMIZE, 0, NULL);

        /* Setup parent class' MMBroadbandModemClass */
        g_assert (MM_SHARED_ROLLING_GET_INTERFACE (self)->peek_parent_broadband_modem_class);
        priv->broadband_modem_class_parent = MM_SHARED_ROLLING_GET_INTERFACE (self)->peek_parent_broadband_modem_class (self);

        /* Setup parent class' MMIfaceModem3gpp */
        g_assert (MM_SHARED_ROLLING_GET_INTERFACE (self)->peek_parent_3gpp_interface);
        priv->iface_modem_3gpp_parent = MM_SHARED_ROLLING_GET_INTERFACE (self)->peek_parent_3gpp_interface (self);

        g_object_set_qdata_full (G_OBJECT (self), private_quark, priv, (GDestroyNotify)private_free);
    }

    return priv;
}

/*****************************************************************************/

void
mm_shared_rolling_setup_ports (MMBroadbandModem *self)
{
    MMPortSerialAt *ports[2];
    guint           i;
    Private        *priv;

    mm_obj_dbg (self, "setting up ports in rolling modem...");

    priv = get_private (MM_SHARED_ROLLING (self));
    g_assert (priv->broadband_modem_class_parent);
    g_assert (priv->broadband_modem_class_parent->setup_ports);

    /* Parent setup first always */
    priv->broadband_modem_class_parent->setup_ports (self);

    ports[0] = mm_base_modem_peek_port_primary   (MM_BASE_MODEM (self));
    ports[1] = mm_base_modem_peek_port_secondary (MM_BASE_MODEM (self));

    for (i = 0; i < G_N_ELEMENTS (ports); i++) {
        if (!ports[i])
            continue;
        mm_port_serial_at_add_unsolicited_msg_handler (
            ports[i],
            priv->sim_ready_regex,
            NULL, NULL, NULL);
    }
}

/*****************************************************************************/

typedef struct {
    MMBearerProperties *config;
    gboolean            initial_eps_off_on;
    GError             *saved_error;
} SetInitialEpsBearerSettingsContext;

static void
set_initial_eps_bearer_settings_context_free (SetInitialEpsBearerSettingsContext *ctx)
{
    g_clear_error (&ctx->saved_error);
    g_clear_object (&ctx->config);
    g_slice_free (SetInitialEpsBearerSettingsContext, ctx);
}

static void
set_initial_eps_bearer_settings_complete (GTask *task)
{
    SetInitialEpsBearerSettingsContext *ctx;

    ctx = g_task_get_task_data (task);
    if (ctx->saved_error)
        g_task_return_error (task, g_steal_pointer (&ctx->saved_error));
    else
        g_task_return_boolean (task, TRUE);
    g_object_unref (task);
}

gboolean
mm_shared_rolling_set_initial_eps_bearer_settings_finish (MMIfaceModem3gpp  *self,
                                                          GAsyncResult      *res,
                                                          GError           **error)
{
    return g_task_propagate_boolean (G_TASK (res), error);
}

static void
after_attach_apn_modem_power_up_ready (MMIfaceModem *self,
                                       GAsyncResult *res,
                                       GTask        *task)
{
    SetInitialEpsBearerSettingsContext *ctx;
    g_autoptr(GError)                   error = NULL;

    ctx = g_task_get_task_data (task);

    if (!mm_iface_modem_set_power_state_finish (self, res, &error)) {
        mm_obj_warn (self, "failed to power up modem after attach APN settings update: %s", error->message);
        if (!ctx->saved_error)
            ctx->saved_error = g_steal_pointer (&error);
    } else
        mm_obj_dbg (self, "success toggling modem power up after attach APN");

    set_initial_eps_bearer_settings_complete (task);
}

static void
parent_set_initial_eps_bearer_settings_ready (MMIfaceModem3gpp *self,
                                              GAsyncResult     *res,
                                              GTask            *task)
{
    SetInitialEpsBearerSettingsContext *ctx;
    Private                            *priv;

    ctx = g_task_get_task_data (task);
    priv = get_private (MM_SHARED_ROLLING (self));

    if (!priv->iface_modem_3gpp_parent->set_initial_eps_bearer_settings_finish (self, res, &ctx->saved_error))
        mm_obj_warn (self, "failed to update APN settings: %s", ctx->saved_error->message);

    if (ctx->initial_eps_off_on) {
        mm_obj_dbg (self, "toggle modem power up after attach APN");
        mm_iface_modem_set_power_state (MM_IFACE_MODEM (self),
                                        MM_MODEM_POWER_STATE_ON,
                                        (GAsyncReadyCallback) after_attach_apn_modem_power_up_ready,
                                        task);
        return;
    }

    set_initial_eps_bearer_settings_complete (task);
}

static void
parent_set_initial_eps_bearer_settings (GTask *task)
{
    MMSharedRolling                    *self;
    SetInitialEpsBearerSettingsContext *ctx;
    Private                            *priv;

    self = g_task_get_source_object (task);
    ctx  = g_task_get_task_data (task);
    priv = get_private (self);

    g_assert (priv->iface_modem_3gpp_parent);
    g_assert (priv->iface_modem_3gpp_parent->set_initial_eps_bearer_settings);
    g_assert (priv->iface_modem_3gpp_parent->set_initial_eps_bearer_settings_finish);

    priv->iface_modem_3gpp_parent->set_initial_eps_bearer_settings (MM_IFACE_MODEM_3GPP (self),
                                                                    ctx->config,
                                                                    (GAsyncReadyCallback)parent_set_initial_eps_bearer_settings_ready,
                                                                    task);
}

static void
before_attach_apn_modem_power_down_ready (MMIfaceModem *self,
                                          GAsyncResult *res,
                                          GTask        *task)
{
    GError *error = NULL;

    if (!mm_iface_modem_set_power_state_finish (self, res, &error)) {
        mm_obj_warn (self, "failed to power down modem before attach APN settings update: %s", error->message);
        g_task_return_error (task, error);
        g_object_unref (task);
        return;
    }
    mm_obj_dbg (self, "success toggling modem power down before attach APN");

    parent_set_initial_eps_bearer_settings (task);
}

void
mm_shared_rolling_set_initial_eps_bearer_settings (MMIfaceModem3gpp    *self,
                                                   MMBearerProperties  *config,
                                                   GAsyncReadyCallback  callback,
                                                   gpointer             user_data)
{
    SetInitialEpsBearerSettingsContext *ctx;
    GTask                              *task;
    MMPortMbim                         *port;

    task = g_task_new (self, NULL, callback, user_data);

    /* This shared logic is only expected in MBIM capable devices */
    g_assert (MM_IS_BROADBAND_MODEM_MBIM (self));
    port = mm_broadband_modem_mbim_peek_port_mbim (MM_BROADBAND_MODEM_MBIM (self));
    if (!port) {
        g_task_return_new_error (task, MM_CORE_ERROR, MM_CORE_ERROR_FAILED,
                                 "No valid MBIM port found");
        g_object_unref (task);
        return;
    }

    ctx = g_slice_new0 (SetInitialEpsBearerSettingsContext);
    ctx->config = g_object_ref (config);
    ctx->initial_eps_off_on = mm_kernel_device_get_property_as_boolean (mm_port_peek_kernel_device (MM_PORT (port)), "ID_MM_ROLLING_INITIAL_EPS_OFF_ON");
    g_task_set_task_data (task, ctx, (GDestroyNotify)set_initial_eps_bearer_settings_context_free);

    if (ctx->initial_eps_off_on) {
        mm_obj_dbg (self, "toggle modem power down before attach APN");
        mm_iface_modem_set_power_state (MM_IFACE_MODEM (self),
                                        MM_MODEM_POWER_STATE_LOW,
                                        (GAsyncReadyCallback) before_attach_apn_modem_power_down_ready,
                                        task);
        return;
    }

    parent_set_initial_eps_bearer_settings (task);
}

/*****************************************************************************/

MMFirmwareUpdateSettings *
mm_shared_rolling_firmware_load_update_settings_finish (MMIfaceModemFirmware  *self,
                                                        GAsyncResult          *res,
                                                        GError               **error)
{
    return g_task_propagate_pointer (G_TASK (res), error);
}

static gboolean
rolling_is_fastboot_supported (MMBaseModem *modem,
                               MMPort      *port)
{
    return mm_kernel_device_get_global_property_as_boolean (mm_port_peek_kernel_device (port), "ID_MM_ROLLING_FASTBOOT");
}

static MMModemFirmwareUpdateMethod
rolling_get_firmware_update_methods (MMBaseModem *modem,
                                     MMPort      *port)
{
    MMModemFirmwareUpdateMethod update_methods;

    update_methods = MM_MODEM_FIRMWARE_UPDATE_METHOD_NONE;

    if (rolling_is_fastboot_supported (modem, port))
        update_methods |= MM_MODEM_FIRMWARE_UPDATE_METHOD_FASTBOOT;

    return update_methods;
}

static void
rolling_at_port_get_firmware_version_ready (MMBaseModem  *self,
                                            GAsyncResult *res,
                                            GTask        *task)
{
    MMFirmwareUpdateSettings    *update_settings;
    MMModemFirmwareUpdateMethod  update_methods;
    const gchar                 *version_from_at;
    g_auto(GStrv)                version = NULL;
    g_autoptr(GPtrArray)         ids = NULL;
    GError                      *error = NULL;

    update_settings = g_task_get_task_data (task);
    update_methods = mm_firmware_update_settings_get_method (update_settings);

    /* Set device ids */
    ids = mm_iface_firmware_build_generic_device_ids (MM_IFACE_MODEM_FIRMWARE (self), &error);
    if (error) {
        mm_obj_warn (self, "failed to build generic device ids: %s", error->message);
        g_task_return_error (task, error);
        g_object_unref (task);
        return;
    }

    /* The version get from the AT needs to be formatted
     *
     * version_from_at : AT+GTPKGVER: "19500.0000.00.01.02.80_6001.0000.007.000.075_A89"
     * version[1] : 19500.0000.00.01.02.80_6001.0000.007.000.075_A89
     */
    version_from_at = mm_base_modem_at_command_finish (self, res, NULL);
    if (version_from_at) {
        version = g_strsplit (version_from_at, "\"", -1);
        if (version && version[0] && version[1] && g_utf8_validate (version[1], -1, NULL)) {
            mm_firmware_update_settings_set_version (update_settings, version[1]);
        }
    }

    mm_firmware_update_settings_set_device_ids (update_settings, (const gchar **)ids->pdata);

    /* Set update methods */
    if (update_methods & MM_MODEM_FIRMWARE_UPDATE_METHOD_FASTBOOT) {
        /* Fastboot AT */
        mm_firmware_update_settings_set_fastboot_at (update_settings, "AT+SYSCMD=\"sys_reboot bootloader\"");
    }

    g_task_return_pointer (task, g_object_ref (update_settings), g_object_unref);
    g_object_unref (task);
}

void
mm_shared_rolling_firmware_load_update_settings (MMIfaceModemFirmware *self,
                                                 GAsyncReadyCallback   callback,
                                                 gpointer              user_data)
{
    GTask *task;
    MMPortSerialAt *at_port;
    MMModemFirmwareUpdateMethod update_methods;
    MMFirmwareUpdateSettings *update_settings;

    task = g_task_new (self, NULL, callback, user_data);

    at_port = mm_base_modem_peek_best_at_port (MM_BASE_MODEM (self), NULL);
    if (at_port) {
        update_methods = rolling_get_firmware_update_methods (MM_BASE_MODEM (self), MM_PORT (at_port));
        update_settings = mm_firmware_update_settings_new (update_methods);
        g_task_set_task_data (task, update_settings, g_object_unref);

        /* Get modem version by AT */
        mm_base_modem_at_command (MM_BASE_MODEM (self),
                                  "+GTPKGVER?",
                                  3,
                                  TRUE,
                                  (GAsyncReadyCallback) rolling_at_port_get_firmware_version_ready,
                                  task);

        return;
    }

    g_task_return_new_error (task,
                             MM_CORE_ERROR,
                             MM_CORE_ERROR_FAILED,
                             "Couldn't find a port to fetch firmware info");
    g_object_unref (task);
}

gboolean
mm_shared_rolling_modem_reset_finish (MMIfaceModem  *self,
                                        GAsyncResult  *res,
                                        GError       **error)
{
    return g_task_propagate_boolean (G_TASK (res), error);
}

static void
ms_basic_connect_extensions_device_reset_set_ready (MbimDevice   *device,
                                                    GAsyncResult *res,
                                                    GTask        *task)
{
    MbimMessage      *response;
    GError           *error = NULL;

    response = mbim_device_command_finish (device, res, &error);
    if (!response || !mbim_message_response_get_result (response, MBIM_MESSAGE_TYPE_COMMAND_DONE, &error)) {
        g_task_return_error (task, error);
        g_object_unref (task);
    } else {
        g_task_return_boolean (task, TRUE);
        g_object_unref (task);
    }

    if (response)
        mbim_message_unref (response);
}

static gboolean
peek_device (gpointer              self,
             MbimDevice          **o_device,
             GAsyncReadyCallback   callback,
             gpointer              user_data)
{
    MMPortMbim *port;

    port = mm_broadband_modem_mbim_peek_port_mbim (MM_BROADBAND_MODEM_MBIM (self));
    if (!port) {
        g_task_report_new_error (self, callback, user_data, peek_device,
                                 MM_CORE_ERROR, MM_CORE_ERROR_FAILED, "Couldn't peek MBIM port");
        return FALSE;
    }

    *o_device = mm_port_mbim_peek_device (port);
    return TRUE;
}

void
mm_shared_rolling_modem_reset (MMIfaceModem        *_self,
                                 GAsyncReadyCallback  callback,
                                 gpointer             user_data)
{
    MMBroadbandModemMbim *self = MM_BROADBAND_MODEM_MBIM (_self);
    GTask                *task;
    MbimDevice           *device;
    MbimMessage          *message;

    if (!peek_device (self, &device, callback, user_data))
        return;

    task = g_task_new (self, NULL, callback, user_data);

    /* This message is defined in the Intel Firmware Update service, but it
     * really is just a standard modem reboot. */
    message = mbim_message_ms_basic_connect_extensions_device_reset_set_new (NULL);
    mbim_device_command (device,
                         message,
                         10,
                         NULL,
                         (GAsyncReadyCallback)ms_basic_connect_extensions_device_reset_set_ready,
                         task);
    mbim_message_unref (message);
}

/*****************************************************************************/

static void
shared_rolling_init (gpointer g_iface)
{
}

GType
mm_shared_rolling_get_type (void)
{
    static GType shared_rolling_type = 0;

    if (!G_UNLIKELY (shared_rolling_type)) {
        static const GTypeInfo info = {
            sizeof (MMSharedRolling),  /* class_size */
            shared_rolling_init,       /* base_init */
            NULL,                      /* base_finalize */
        };

        shared_rolling_type = g_type_register_static (G_TYPE_INTERFACE, "MMSharedRolling", &info, 0);
        g_type_interface_add_prerequisite (shared_rolling_type, MM_TYPE_IFACE_MODEM);
        g_type_interface_add_prerequisite (shared_rolling_type, MM_TYPE_IFACE_MODEM_3GPP);
    }

    return shared_rolling_type;
}
